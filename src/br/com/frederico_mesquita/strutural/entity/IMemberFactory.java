package br.com.frederico_mesquita.strutural.entity;

public interface IMemberFactory {
	IMembership subscribe(SubscriptionType subscriptionType, String name);
}
