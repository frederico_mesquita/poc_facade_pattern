package br.com.frederico_mesquita.strutural;

import java.time.Instant;

import br.com.frederico_mesquita.strutural.service.CarBooking;
import br.com.frederico_mesquita.strutural.service.CarModel;
import br.com.frederico_mesquita.strutural.service.FlightBooking;
import br.com.frederico_mesquita.strutural.service.HotelBooking;
import br.com.frederico_mesquita.strutural.service.RoomType;

public class TravelFacade {
	private static TravelFacade _instance;
	
	private TravelFacade() {}
	
	public static TravelFacade getInstance() {
		if(null == _instance) {
			_instance = new TravelFacade();
		}
		return _instance;
	}
	
	public int BookPackage(
				String guestName, String fromLocation, String toLocation, Instant instant, String flightNumber,
				Instant instant2, Instant instant3, CarModel carModel,
				Instant instant4, Instant instant5, RoomType roomType) {
		int count = 0;
		
		count = FlightBooking.getInstance().BookFlight(guestName, fromLocation, toLocation, instant, flightNumber);
		if(count > 0) {
			count = FlightBooking.getInstance().BookFlight(guestName, toLocation, fromLocation, instant, flightNumber);
			if(count > 0) {
				count = HotelBooking.getInstance().BookHotel(guestName, instant4, instant5, roomType);
				if(count > 0) {
					count = CarBooking.getInstance().BookCar(guestName, instant2, instant3, carModel);
				}
			}
		}
		
		return count;
	}
	
}
