package br.com.frederico_mesquita.strutural.service;

import java.time.Instant;

public class HotelBooking {
	private static HotelBooking _instance = null;
	private static int count = 0;
	
	private HotelBooking() {}
	
	public static HotelBooking getInstance() {
		if (null == _instance) {
			_instance = new HotelBooking();
		}
		count++;
		return _instance;
	}
	
	public int BookHotel(String guestName, Instant instant4, Instant instant5, RoomType roomType) {
		int iReturn = count;
		
		System.out.println("Flight booking for " + guestName + " done.\n");
		System.out.println(
			"Car details:" + 
					"\nStart date: " + instant4.toString() + 
					"\nEnd date: " + instant5.toString() + 
				"\nRoom type: " + roomType.toString()
		);
		
		return iReturn;
	}
}
