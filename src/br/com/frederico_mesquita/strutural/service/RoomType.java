package br.com.frederico_mesquita.strutural.service;

public enum RoomType {
	SINGLE, DOUBLE;
}
