package br.com.frederico_mesquita.strutural.service;

import javax.json.Json;

public class OneToOneTraining extends Service {

	public OneToOneTraining(IService _iService) {
		super(_iService);
	}

	@Override
	public String getDescription() {
		return super._iService.getDescription() + ServiceMessage.ONE_TO_ONE_TRAINNING_SERVICE_DESCRIPTION.getMessage();
	}

	@Override
	public double getCost() {
		return super._iService.getCost() + ServiceCost.ONE_TO_ONE_TRAINING_SERVICE_COST.getCost();
	}
	
	public String toString() {
		return Json.createObjectBuilder()
			.add("consumedService", this.getDescription())
			.add("cost", this.getCost()).build().toString();
	}

	public void showService() {
		System.out.println(this.toString());
	}

}
