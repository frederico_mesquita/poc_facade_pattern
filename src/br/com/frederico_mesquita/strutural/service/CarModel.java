package br.com.frederico_mesquita.strutural.service;

public enum CarModel {
	HATCH, SEDAN, SPORT;
}
