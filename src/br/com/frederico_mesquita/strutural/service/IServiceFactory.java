package br.com.frederico_mesquita.strutural.service;

public interface IServiceFactory {
	IService subscribe(MemberType memberType, ServiceSubscriptionType serviceSubscriptionType);
	IService subscribe(MemberType memberType, ServiceSubscriptionType serviceSubscriptionType, IService iService);
}
