package br.com.frederico_mesquita.strutural.service;

import java.time.Instant;

public class CarBooking {

	private static CarBooking _instance = null;
	private static int count = 0;
	
	private CarBooking() {}
	
	public static CarBooking getInstance() {
		if (null == _instance) {
			_instance = new CarBooking();
		}
		count++;
		return _instance;
	}
	
	public int BookCar(String guestName, Instant instant2, Instant instant3, CarModel carModel) {
		int iReturn = count;
		
		System.out.println("Flight booking for " + guestName + " done.\n");
		System.out.println(
			"Car details:" + 
					"\nStart date: " + instant2.toString() + 
					"\nEnd date: " + instant3.toString() + 
				"\nCar model: " + carModel.toString()
		);
		
		return iReturn;
	}
}
