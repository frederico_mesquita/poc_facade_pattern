package br.com.frederico_mesquita.strutural.service;

import java.time.Instant;

public class FlightBooking {

	private static FlightBooking _instance = null;
	private static int count = 0;
	
	private FlightBooking() {}
	
	public static FlightBooking getInstance() {
		if (null == _instance) {
			_instance = new FlightBooking();
		}
		count++;
		return _instance;
	}
	
	public int BookFlight(String guestName, String fromLocation, String toLocation, Instant instant, String flightNumber) {
		int iReturn = count;
		
		System.out.println("Flight booking for " + guestName + " done.\n");
		System.out.println(
			"Travel details:" + 
				"\nFrom location: " + fromLocation +
				"\nTo location:" + toLocation + 
				"\nTravel date: " + instant.toString() + 
				"\nFlight number: " + flightNumber
		);
		
		return iReturn;
	}
}
